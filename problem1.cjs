const fs = require("fs");

function createJsonFilesAndDelete() {
  const path = "./json";
  fs.mkdir(path, { recursive: true }, (err) => {
    if (err) throw err;

    const numFiles = 8;
    for (let i = 0; i < numFiles; i++) {
      const fileName = `${path}/${Math.random().toString(36).slice(-5)}.json`;
      const data = JSON.stringify({ randomData: Math.random() });

      fs.writeFile(fileName, data, (err) => {
        if (err) {
          throw err;
        } else {
          console.log(`File created successfully: ${fileName}`);
        }

        fs.unlink(fileName, (err) => {
          if (err) {
            throw err;
          } else {
            console.log(`File deleted successfully: ${fileName}`);
          }
        });
      });
    }
  });
}

module.exports = createJsonFilesAndDelete;