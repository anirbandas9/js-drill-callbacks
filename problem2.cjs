const fs = require("fs");

function problem2() {
    fs.readFile("lipsum.txt", "utf-8", (err, data) => {
        // console.log(data);
        const path = "upperCase.txt";
        const storeFilePath = "filenames.txt";
        const upperCaseData = data.toUpperCase();

        // 2. Convert the content to uppercase and write to a new file.
        fs.writeFile(path, upperCaseData, (err) => {
            if (err) throw err;
            console.log(`The file created successfully: ${path}`);

            fs.appendFile(storeFilePath, `${path}\n`, (err) => {
                if (err) throw err;
                console.log(`The file stored successfully in ${storeFilePath}`);
                
                // 3. Read the new file and convert it to lowercase.
                fs.readFile(path, "utf-8", (err, upperCaseReadData) => {
                    // console.log(upperCaseData);
                    const newPath = "lowerCaseSentences.txt";
                    // Then split the contents into sentences.
                    const lowerCaseArr = data.toLowerCase().split(". ");
                    const splitContent = lowerCaseArr.reduce((acc, data) => {
                        acc += `\n${data}`;
                        return acc;
                    }, "");
                    // console.log(splitContent);
                    // Then write it to a new file.
                    fs.writeFile(newPath, splitContent, (err) => {
                        if (err) throw err;
                        console.log(`The file created successfully: ${newPath}`);

                        // Store the name of the new file in filenames.txt
                        fs.appendFile(storeFilePath, `${newPath}\n`, (err) => {
                            if (err) throw err;
                            console.log(`The file stored successfully in ${storeFilePath}`);

                            // 4. Read the new files
                            fs.readFile(path, "utf-8", (err, data1) => {
                                fs.readFile(newPath, "utf-8", (err, data2) => {
                                    // sort the content
                                    const sortContent = `${data1 + data2}`
                                        .split("\n")
                                        .sort()
                                        .join("\n");
                                    // console.log(sortContent);
                                    const newPath2 = "sorted.txt";
                                    fs.writeFile(newPath2, sortContent, (err) => {
                                        if (err) throw err;
                                        console.log(`The file created successfully: ${newPath2}`);

                                        // Store the name of the new file in filenames.txt
                                        fs.appendFile(storeFilePath, newPath2, (err) => {
                                            if (err) throw err;
                                            console.log(`The file stored successfully in ${storeFilePath}`);

                                            // 5. Read the contents of filenames.txt
                                            fs.readFile(storeFilePath, "utf-8", (err, fileNamesData) => {
                                                // delete all the new files that are mentioned in that list
                                                function deletedFiles() {
                                                    return `${fileNamesData}\n${storeFilePath}`.split("\n").map((file) => {
                                                        fs.unlink(file, (err) => {
                                                            if (err) throw err;
                                                            console.log(`The file deleted successfully: ${file}`);
                                                        });
                                                    });
                                                }
                                                deletedFiles();
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

module.exports = problem2;